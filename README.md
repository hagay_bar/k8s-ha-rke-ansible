**# k8s-ha-rke-ansible**

**This ansible role will install full local k8s cluster with rke cert manager and rancher platform via ansible**

**In order to run it make sure you have this prerequisites**

1) Six vm's. 1 to serve as rke terminal 2 masters and 3 nodes ( you can change the quantity via the cluster.yml file and cluster variable files.)
2) Make sure you have clean ubuntu 18.04 server installed on all of them
3) Lan connection is valid between all nodes ( check that ping runs between them )
4) Ansible is installed on your terminal machine

**In order to run this role and deploy the cluster**

1) Download this repo to your terminal machine
2) Cd to Tasks folder
3) Edit in /vars your ENV variables, network id's , ip's, and cluster parameters.
3) Run this command as root:  ansible-playbook -i inventory main.yml --ask-pass --extra-vars='pubkey="<pubkey>"'    ( in the <pubkey> make sure to paste you ssh pub key )
4) Make sure to check process has ended without any errors.
5) Run kubectl get pods --all-namespaces -o wide on terminal to make sure all relevant roles were installed properly.
6) run kubectl to verify all namespaces are created and pods are configured propely.
7) access the rancher url and make sure you can see all cluster nodes.

if you have any questions or bug reports please send them to me at **hagay_bar@outlook.com**